<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\User;

class RbacController extends Controller
{
    public function actionInit()
    {

//        $master = new User();
//        $master->username = 'master';
//        $master->email = '$manager';
//        $master->setPassword('master');
//        $master->generateAuthKey();
//        $master->save();
//
//        $manager = new User();
//        $manager->username = 'manager';
//        $manager->email = 'manager';
//        $manager->setPassword('manager');
//        $manager->generateAuthKey();
//        $manager->save();

        $auth = Yii::$app->authManager;

        $auth->removeAll();
        // добавляем разрешение
        $viewPanel = $auth->createPermission('viewPanel');
        $viewPanel->description = 'Просмотр панели администратора';
        $auth->add($viewPanel);
        $addOwnOrder = $auth->createPermission('addOwnOrder');
        $addOwnOrder->description = 'Добавдение своего выполненного заказа';
        $auth->add($addOwnOrder);
        $viewOwnStatistics = $auth->createPermission('viewOwnStatistics');
        $viewOwnStatistics->description = 'Просмотр собственной статистики';
        $auth->add($viewOwnStatistics);

        $viewLeftAside = $auth->createPermission('viewLeftAside');
        $viewLeftAside->description = 'Просмотр левой панели админки';
        $auth->add($viewLeftAside);

        // добавляем роль
        $master = $auth->createRole('master');
        $master->description = 'Мастер';
        $auth->add($master);
        $manager = $auth->createRole('manager');
        $manager->description = 'Менеджер';
        $auth->add($manager);
        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $auth->add($admin);

        //даём роли разрешение
        $auth->addChild($master, $viewOwnStatistics);
        $auth->addChild($master, $addOwnOrder);

        $auth->addChild($manager, $viewPanel);
        $auth->addChild($manager, $viewLeftAside);

        $auth->addChild($admin, $master);
        $auth->addChild($admin, $manager);

        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        // обычно реализуемый в модели User.
        $auth->assign($master, 2);
        $auth->assign($admin, 1);
        $auth->assign($manager, 3);
    }
}