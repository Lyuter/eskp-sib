<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $contractor_id
 * @property string $date_add
 * @property string $date_complete
 * @property integer $price
 * @property integer $salary
 * @property integer $done
 *
 * @property Contractors $contractor
 * @property Customers $customer
 * @property OrdersServices[] $ordersServices
 * @property Services[] $services
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'contractor_id', 'price', 'done', 'salary'], 'required'],
            [['customer_id', 'contractor_id', 'price', 'done', 'salary'], 'integer'],
            [['date_add', 'date_complete'], 'safe'],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractors::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Заказчик',
            'contractor_id' => 'Мастер',
            'date_add' => 'Дата добавления',
            'date_complete' => 'Дата завершения',
            'price' => 'Цена',
            'salary' => 'Себе',
            'done' => 'Завершено?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractors::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersServices()
    {
        return $this->hasMany(OrdersServices::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Services::className(), ['id' => 'service_id'])->viaTable('orders_services', ['order_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            OrdersServices::deleteAll(['order_id' => $this->id]);
            return true;
        } else {
            return false;
        }
    }
}
