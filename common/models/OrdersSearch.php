<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form of `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    public $customer;
    public $contractor;
    public $dateRange;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'contractor_id', 'done'], 'integer'],
            [['date_add', 'date_complete'], 'safe'],
            [['customer', 'contractor', 'dateRange'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find()->joinWith(['customer', 'contractor']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['customer'] = [
            'asc' => ['customers.fullname' => SORT_ASC],
            'desc' => ['customers.fullname' => SORT_DESC],
        ];


        $dataProvider->sort->attributes['contractor'] = [
            'asc' => ['contractors.fullname' => SORT_ASC],
            'desc' => ['contractors.fullname' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'customer_id' => $this->customer_id,
            'contractor_id' => $this->contractor_id,
            'salary' => $this->salary,
            'done' => $this->done,
        ]);

        $query->andFilterWhere(['like', 'services', $this->services])
            ->andFilterWhere(['like', 'contractors.fullname', $this->contractor])
            ->andFilterWhere(['like', 'customers.fullname', $this->customer])
        ;

        if(!empty($this->dateRange) && strpos($this->dateRange, '-') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->dateRange);
            $query->andFilterWhere(['between', 'date_add', $start_date, $end_date]);
        }

        return $dataProvider;
    }
}
