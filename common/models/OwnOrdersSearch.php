<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OwnOrdersSearch represents the model behind the search form of `common\models\Orders`.
 */
class OwnOrdersSearch extends Orders
{
    public $customer;
    public $dateRange;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['date_add', 'date_complete'], 'safe'],
            [['customer', 'dateRange'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find()->where(['contractor_id' => User::getContractor()->id])->joinWith(['customer', 'contractor']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['customer'] = [
            'asc' => ['customers.fullname' => SORT_ASC],
            'desc' => ['customers.fullname' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'customer_id' => $this->customer_id,
//            'date_add' => $this->date_add,
            'date_complete' => $this->date_complete,
            'price' => $this->price,
            'salary' => $this->price,
            'done' => $this->done,
        ]);

        $query->andFilterWhere(['like', 'services', $this->services])
            ->andFilterWhere(['like', 'customers.fullname', $this->customer])
//            ->andFilterWhere(['>=', 'date_add', $this->date_add])
        ;


        if(!empty($this->dateRange) && strpos($this->dateRange, '-') !== false) {
            list($start_date, $end_date) = explode(' - ', $this->dateRange);
            $query->andFilterWhere(['between', 'date_add', $start_date, $end_date]);
        }

        return $dataProvider;
    }
}
