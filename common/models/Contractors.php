<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contractors".
 *
 * @property integer $id
 * @property string $fullname
 * @property string $phone
 *
 * @property Orders[] $orders
 */
class Contractors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contractors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname'], 'required'],
            [['fullname', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Ф.И.О.',
            'phone' => 'Телефон'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['contractor_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            if (Orders::find()->where(['contractor_id' => $this->id])->exists())
            {
                Yii::$app->session->setFlash('error', "Мастер $this->fullname является исполнителем заказов! Удалите выполненные им заказы");
                return false;
            }
            return true;

        }
        else {
            return false;
        }
    }
}
