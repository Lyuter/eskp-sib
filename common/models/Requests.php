<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "requests".
 *
 * @property string $id
 * @property string $date
 * @property string $name
 * @property string $phone
 * @property string $description
 * @property integer $done
 */
class Requests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['name', 'phone'], 'string'],
            [['done'], 'required'],
            [['done'], 'integer'],
            [['description'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'name' => 'Клиент',
            'phone' => 'Телефон',
            'description' => 'Описание проблемы',
            'done' => 'Успешно?',
        ];
    }
}
