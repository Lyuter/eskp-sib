<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property integer $id
 * @property string $fullname
 * @property string $address
 * @property string $phone
 * @property string $coordinates

 *
 * @property Orders[] $orders
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname'], 'required'],
            [['fullname'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 500],
            [['phone'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Ф.И.О.',
            'address' => 'Адрес',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['customer_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            if (Orders::find()->where(['customer_id' => $this->id])->exists())
            {
                Yii::$app->session->setFlash('error', "Клиент $this->fullname является заказчиком! Удалите заказы с его участием.");
                return false;
            }
            return true;

        }
        else {
            return false;
        }
    }

    /**
     * Getting and saving the coordinates of the customer
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $address_json = file_get_contents("https://geocode-maps.yandex.ru/1.x/?format=json&geocode=Красноярск+" . $this->address . "&results=1");
            $address = json_decode($address_json);
            $this->coordinates = $address->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
            return true;
        } else {
            return false;
        }
    }
    public function fields()
    {
        return [
            'fullname',
            'phone',
            'coordinates'
        ];
    }
}
