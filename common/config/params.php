<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'eskp.robot@ya.ru',
    'user.passwordResetTokenExpire' => 3600,
];
