<?php

/* @var $this yii\web\View */
/* @var $cityProperty array */

$this->title = 'Единая служба компьютерной помощи';
$this->params['cityProperty'] = $cityProperty;
?>

<section id="services">
    <div class="container">
        <div class="heading wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="100ms">
            <h1 class="text-center text-uppercase blue">Наши услуги</h1>

            <div class="row promo">
                <div class="col-lg-2 col-md-3 col-xs-6 wow fadeInUp" data-wow-duration="800ms" data-wow-delay="100ms">
                    <div class="promo-item">
                        <div class="promo_1 promo-image"></div>
                        <div class="promo-text">
                            <h4>Принимаем заявки 24 часа!</h4>
                            <!--<p><a href="tel: +7(391)286-04-412">+7(391)286-04-41</a></p>-->
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 wow fadeInUp" data-wow-duration="800ms" data-wow-delay="200ms">
                    <div class="promo-item">
                        <div class="promo_2 promo-image"></div>
                        <div class="promo-text">
                            <h4>Выезд к Вам в офис или домой</h4>
                            <!--<p>Курьер выезжает к вам в течении 1 часа</p>-->
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 wow fadeInUp" data-wow-duration="800ms" data-wow-delay="300ms">
                    <div class="promo-item">
                        <div class="promo_3 promo-image"></div>
                        <div class="promo-text">
                            <h4>Диагностика вашего устройства</h4>
                            <!--<p>Происходит в день обращения к нам</p>-->
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 wow fadeInUp" data-wow-duration="800ms" data-wow-delay="400ms">
                    <div class="promo-item">
                        <div class="promo_4 promo-image"></div>
                        <div class="promo-text">
                            <h4>Согласование работ с заказчиком</h4>
                            <!--<p>Прозрачная цена, работа по прайс-листу</p>-->
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-md-offset-3 col-lg-offset-0 col-xs-6 wow fadeInUp" data-wow-duration="800ms" data-wow-delay="500ms">
                    <div class="promo-item">
                        <div class="promo_5 promo-image"></div>
                        <div class="promo-text">
                            <h4>Гарантия на ремонт</h4>
                            <!--<p>С использованием оригинальных запчастей</p>-->
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 wow fadeInUp" data-wow-duration="800ms" data-wow-delay="500ms">
                    <div class="promo-item">
                        <div class="promo_6 promo-image"></div>
                        <div class="promo-text">
                            <h4>Проверка и доставка до вас</h4>
                            <!--<p>После контроля качества отправляем устройство к вам</p>-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="row promo">
                <div class="text-left col-md-6">
                    <p class="description text-justify">Ремонт, настройка или модернизация компьютера выполняются прямо у Вас дома, либо в офисе. Достаточно лишь связаться с нами, описать примерную проблему и наш специалист приедет в любое удобное для Вас время. При необходимости более сложного ремонта наш специалист доставит оборудование в сервис, а после ремонта привезет его обратно.</p>
                </div>
                <div class="text-center col-md-6 ">
                    <div class="load-more wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="150ms">
                        <a data-remodal-target="priceModal" class="btn btn-loadmore"><i class="fa fa-list"></i> Цены на услуги</a>
                        <p>*Не нашли нужную информацию? Свяжитесь с нами для консультации <br><?= $cityProperty['phone'] ?></p>
                    </div>
                </div>
        </div>
        <hr class="stripe">
        <div class="text-center our-services hidden-xs">
            <div class="row">
                <div class="col-sm-3" data-content="Операционная система представляет собой своего рода скелет, на котором держатся современные возможности компьютеров. Без нее компьютер всего лишь очень дорогая коробочка и очень обидно, когда она не работает так, как мы этого хотим." data-price="Стоимость - от 500 руб. *" data-title="Установка Windows, iOS, Android и все необходимые программы и драйвера">
                    <div class="service-icon service-iconse-1">
                    </div>
                    <div class="service-info">
                        <p>Установка Windows, iOS, Android и все необходимые программы и драйвера</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="Современную жизнь трудно представить без сети Интернет. Там мы находим полезную информацию, работу, развлечения, поддерживаем связь с близкими нам людьми. Мы поможем Вам не потерять столь полезный и важный инструмент." data-title="Настройка сети Internet, Wi-Fi, роутера" data-price="Стоимость - от 500 руб. *">
                    <div class="service-icon service-iconse-3">
                    </div>
                    <div class="service-info">
                        <p>Настройка сети Internet, Wi-Fi, роутера</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="Никто не застрахован, что в самый нужный момент Ваш ноутбук или ПК начнет шуметь, греться или вообще не включится. Что тогда делать? Правильно, не паниковать. Бежать в магазин и тратить кучу денег необязательно. Чаще всего неполадка вызвана далеко не фатальными вещами и все устраняется легко и просто. А самое главное дешевле, чем стоимость нового компьютера!" data-title="Ремонт ноутбуков и системных блоков" data-price="Стоимость - от 480 руб. *">
                    <div class="service-icon service-iconse-8">
                    </div>
                    <div class="service-info">
                        <p>Ремонт ноутбуков и системных блоков</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="Кто же не слышал о компьютерных вирусах, баннерах, троянах, смс мошенниках и т.д.? Наши специалисты готовы встать на защиту ваших персональных данных, уберечь Вас от мошенников." data-title="Удаление вирусов, баннеров. Установка Антивируса" data-price="Стоимость - от 500 руб. *">
                    <div class="service-icon service-iconse-12">
                    </div>
                    <div class="service-info">
                        <p>Удаление вирусов, банеров. Установка Антивируса</p>
                    </div>
                </div>
            </div>
            <hr class="stripe">
            <div class="row">
                <div class="col-sm-3" data-content="Современная жизнь - это движение и бесконечный поток информации, которую мы, отчасти, черпаем из экранов мониторов, ноутбуков, планшетов, ПК. И очень обидно, если этот экран перестает работать. Эту неприятность мы можем легко устранить." data-title="Замена сломанных экранов" data-price="Стоимость - от 800 руб. *">
                    <div class="service-icon service-iconse-9">
                    </div>
                    <div class="service-info">
                        <p>Замена сломанных экранов</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="Материнская плата - это своего рода нервная система компьютера. Данные передаются от одного устройства к другому по строго определенным маршрутам. Нарушения работы этого устройства, приведет к тому, что все элементы останутся без связи, питания и своих полезных функций выполнять не смогут." data-title="Ремонт материнских плат" data-price="Стоимость - от 1500 руб. *">
                    <div class="service-icon service-iconse-2">
                    </div>
                    <div class="service-info">
                        <p>Ремонт материнских плат</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="Купить готовый системный блок или собрать его? Какой ноутбук выбрать из такого громадного разнообразия, чтобы не переплатить, но получить от него максимум? На эти и другие вопросы мы с радостью Вам ответим и поможем выбрать оптимальный вариант!" data-title="Подбор необходимых комплектующих для ремонта" data-price="Стоимость - от 400 руб. *">
                    <div class="service-icon service-iconse-6">
                    </div>
                    <div class="service-info">
                        <p>Подбор необходимых комплектующих для ремонта</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="Удобно, когда не выходя из дома можно распечатать фотографию, курсовую или договор, или наоборот сканировать их, для последующего хранения на своем компьютере. Мы поможем Вам в настройки данных устройств." data-title="Настройка принтеров, сканеров" data-price="Стоимость - от 400 руб. *">
                    <div class="service-icon service-iconse-5">
                    </div>
                    <div class="service-info">
                        <p>Настройка принтеров, сканеров</p>
                    </div>
                </div>
            </div>
            <hr class="stripe">
            <div class="row">
                <div class="col-sm-3" data-content="Не имея нужного охлаждения внутри компьютера запускается медленный, но верный, процесс саморазрушения. Повышенная температура приводит к процессам окисления, разрушения внутренней структуры микросхем и чипов. Всё это неминуемо приведёт к поломке рано или поздно. Но зачем ждать этого, если можно устранить проблему в зародыше?" data-title="Ремонт системы охлаждения. Замена термопасты" data-price="Стоимость - от 1000 руб. *">
                    <div class="service-icon service-iconse-10">
                    </div>
                    <div class="service-info">
                        <p>Ремонт системы охлаждения</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="Запыленность внутри компьютера приводит к локальному перегреву микросхем, ухудшению контактов, статическому пробою электричества. Как итог, Вас ожидает трата денег и потеря времени на ремонт. Правильная чистка компьютера - залог долгой работы Вашего компьютера." data-title="Чистка от пыли" data-price="Стоимость - от 300 руб. *">
                    <div class="service-icon service-iconse-4">
                    </div>
                    <div class="service-info">
                        <p>Чистка от пыли</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="Пролитый сок или газировка на клавиатуру, Вам это знакомо? Если нет, то вам повезло. Ну а если да, то мы готовы прийти и устранить эту неприятность." data-title="Чистка после попадания влаги" data-price="Стоимость - от 800 руб. *">
                    <div class="service-icon service-iconse-7">
                    </div>
                    <div class="service-info">
                        <p>Чистка после попадания влаги</p>
                    </div>
                </div>
                <div class="col-sm-3" data-content="В небольших компаниях, офисах зачастую нецелесообразно держать техника, который будет каждый день следить за работоспособностью компьютеров и оргтехники. Чтобы снизить затраты, мы предлагаем доверить эту работу нам." data-title="Аутсорсинг компьютеров и оргтехники в офисах и компаниях" data-price="Расчитывается индивидуально">
                    <div class="service-icon service-iconse-11">
                    </div>
                    <div class="service-info">
                        <p>Аутсорсинг компьютеров и оргтехники в офисах и компаниях</p>
                    </div>
                </div>
            </div>
            <hr class="stripe">
        </div>
    </div>
</section><!--/#services-->

<section id="about-us">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 text-center">
                <div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <h1>Бонусы при заказе через сайт</h1>
                    <p class="description-bold">Сделайте заказ прямо сейчас и абсолютно бесплатно вы <strong>получите Антивирус</strong> и <strong>10% скидку</strong> на выполняемые работы.</p>
                    <div class="">
                        <a class="btn btn-start animated pulse" data-remodal-target="contactModal">Вызов мастера</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs sale"></div>
        </div>
    </div>
</section><!--/#about-us-->

<section id="service-package" class="hidden-xs">
    <div class="container">
        <div class="row">
            <div class="package-logo"></div>
            <div class="text-center col-sm-3">
                <div class="package package-eco" data-remodal-target="ecoPriceModal">
                    <h3 align="center" class="effect-box">- ПОДРОБНЕЕ -</h3>
                </div>
            </div>
            <div class="text-center col-sm-3">
                <div class="package package-standart" data-remodal-target="standartPriceModal">
                    <h3 align="center" class="effect-box">- ПОДРОБНЕЕ -</h3>
                </div>
            </div>
            <div class="text-center col-sm-3">
                <div class="package package-premium" data-remodal-target="premiumPriceModal">
                    <h3 align="center" class="effect-box">- ПОДРОБНЕЕ -</h3>
                </div>
            </div>
            <div class="text-center col-sm-3">
                <div class="package package-vip" data-remodal-target="vipPriceModal">
                    <h3 align="center" class="effect-box">- ПОДРОБНЕЕ -</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="contact">
    <div class="container">
        <div class="col-sm-3">
            <div class="text-center wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                <h2>Как нас найти:</h2>
            </div>
            <div class="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div>
                    <div class="contact-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <ul class="address">
                            <li><i class="fa fa-map-marker"></i> <span> Адрес:</span></li>
                            <li><?= $cityProperty['address'][0] ?></li>
                            <li><i class="fa fa-phone"></i> <span> Телефон:</span></li>
                            <li><a href="#modalContact" data-toggle="modal"><?= $cityProperty['address'][1] ?></a></li>
                            <li><i class="fa fa-envelope"></i> <span> Email:</span></li>
                            <li><a href="mailto:eskp24@gmail.com"> eskp24@gmail.com</a></li>
                            <li><i class="fa fa-globe"></i> <span> Сайт:</span> </li>
                            <li><a href="http://<?= $cityProperty['address'][2] ?>"><?= $cityProperty['address'][2] ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <p class="text-uppercase text-center offer orange">Выезд в пределах города - бесплатно!<br>  <small> Возможен выезд в пригород</small></p>
            <div id="google-map" class="wow fadeIn" data-latitude="<?= $cityProperty['map'][0] ?>" data-longitude="<?= $cityProperty['map'][1] ?>" data-wow-duration="1000ms" data-wow-delay="400ms"></div>
        </div>
    </div>
</section><!--/#contact-->

<div class="logos hidden-xs hidden-sm">

    <div class="logo-item brand-logo-1 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="500ms">
    </div>
    <div class="logo-item brand-logo-2 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="400ms">
    </div>
    <div class="logo-item brand-logo-3 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
    </div>
    <div class="logo-item brand-logo-4 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="200ms">
    </div>
    <div class="logo-item brand-logo-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="100ms">
    </div>
    <div class="logo-item brand-logo-6 wow fadeIn" data-wow-duration="700ms" data-wow-delay="100ms">
    </div>
    <div class="logo-item brand-logo-7 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="100ms">
    </div>
    <div class="logo-item brand-logo-8 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="200ms">
    </div>
    <div class="logo-item brand-logo-9 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
    </div>
    <div class="logo-item brand-logo-10 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="400ms">
    </div>
    <div class="logo-item brand-logo-11 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="500ms">
    </div>
</div>


