<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\widgets\MaskedInput;
$cityProperty = $this->params['cityProperty'];
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="http://www.eskp-sib.ru/favicon.ico" type="image/x-icon">
</head>
<body>
<?php $this->beginBody() ?>

<div class="preloader"><i class="fa fa-circle-o-notch fa-spin"></i></div>

<div class="modals">
    <div class="remodal" data-remodal-id="priceModal">
        <div class="content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button data-remodal-action="cancel" class="close">×</button>
                <h4 class="modal-title">Цены на услуги</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <div class="price-content">
                    <div>
                        <table>
                            <tbody>
                            <tr class="border-tr">
                                <td>Выезд инженера</td>
                                <td>Бесплатно</td>
                            </tr>
                            <tr>
                                <td>Доставка в сервисный центр</td>
                                <td>Бесплатно</td>
                            </tr>
                            <tr>
                                <td>Первичная диагностика неисправностей</td>
                                <td>Бесплатно*</td>
                            </tr>
                            <tr class="white-tr">
                                <td>Установка программного обеспечения</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Установка ОС Microsoft Windows (2000 / XP / Vista / 7 / 8 / 10)</td>
                                <td>500 руб.</td>
                            </tr>
                            <tr>
                                <td>Установка ОС Linux (Ubuntu, OpenSUSE и т. д)</td>
                                <td>500 руб.</td>
                            </tr>
                            <tr>
                                <td>Установка драйвера для внутреннего устройства компьютера</td>
                                <td>200 руб.</td>
                            </tr>
                            <tr>
                                <td>Установка базового пакета программного обеспечения</td>
                                <td>1000 руб.</td>
                            </tr>
                            <tr>
                                <td>Установка Microsoft Office / Open Office</td>
                                <td>550 руб.</td>
                            </tr>
                            <tr>
                                <td>Установка антивируса (freeware)</td>
                                <td>500 руб.</td>
                            </tr>
                            <tr>
                                <td>Проверка компьютера на наличие вирусов и их устранение</td>
                                <td>450-1000 руб.</td>
                            </tr>
                            <tr>
                                <td>Восстановление ОС Microsoft Windows после сбоя или вирусов</td>
                                <td>1000 руб.</td>
                            </tr>
                            <tr>
                                <td>Прочие работы по настройке и оптимизации Windows</td>
                                <td>от 250 руб.</td>
                            </tr>
                            <tr class="white-tr">
                                <td>Настройка сетей</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Подключение и настройка Wi-Fi роутера</td>
                                <td>500-1000 руб.</td>
                            </tr>
                            <tr>
                                <td>Настройка системы для работы в локальной сети и интернет</td>
                                <td>350 руб.</td>
                            </tr>
                            <tr class="white-tr">
                                <td>Ремонт ноутбуков</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Чистка системы охлаждения от пыли (с полным разбором)</td>
                                <td>1000 руб.</td>
                            </tr>
                            <tr>
                                <td>Замена матрицы ноутбука</td>
                                <td>800 руб.</td>
                            </tr>
                            <tr>
                                <td>Замена разъема (питания/USB/LAN/VGA/...) ноутбука</td>
                                <td>от 1000 руб.</td>
                            </tr>
                            <tr>
                                <td>Устранение последствий залития клавиатуры ноутбука</td>
                                <td>от 800 руб.</td>
                            </tr>
                            <tr>
                                <td>Ремонт / Замена шлейфов ноутбука</td>
                                <td>480 руб.</td>
                            </tr>
                            <tr>
                                <td>Замена клавиатуры ноутбука</td>
                                <td>550 руб.</td>
                            </tr>
                            <tr>
                                <td>Ремонт / Замена лампы подсветки</td>
                                <td>1000-2000 руб.</td>
                            </tr>
                            <tr>
                                <td>Ремонт цепи питания материнской платы ноутбука</td>
                                <td>от 1500 руб.</td>
                            </tr>
                            <tr>
                                <td>Прошивка BIOS на рабочем ноутбуке</td>
                                <td>750 руб.</td>
                            </tr>
                            <tr>
                                <td>Прошивка BIOS с полной разборкой</td>
                                <td>от 1200 руб.</td>
                            </tr>
                            <tr>
                                <td>Ремонт / Замена BGA чипов (видеочипа, мостов и др.)</td>
                                <td>от 2500 руб.</td>
                            </tr>

                            <tr class="white-tr">
                                <td>Восстановление информации</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Восстановление данных после нарушения логической структуры</td>
                                <td>950 руб.</td>
                            </tr>
                            <tr>
                                <td>Восстановление данных с накопителей с нечитаемыми секторами</td>
                                <td>от 1000 руб.</td>
                            </tr>
                            <tr>
                                <td>Восстановление с накопителя с разрушенными модулями "служебки"</td>
                                <td>от 1500 руб.</td>
                            </tr>
                            <tr>
                                <td>Снятие пароля с жестких дисков</td>
                                <td>от 850 руб.</td>
                            </tr>
                            <tr>
                                <td>Повреждение блока магнитных головок или коммутатора жёсткого диска</td>
                                <td>от 3500 руб.</td>
                            </tr>
                            <tr>
                                <td>Заклинивание вала двигателя HDD</td>
                                <td>2500 руб.</td>
                            </tr>
                            <tr>
                                <td>Восстановление данных с RAID массивов</td>
                                <td>от 1500 руб.</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="table-hint">
                            * Точную стоимость уточняйте у оператора
                        </div>
                    </div>
                </div>
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-remodal-action="cancel">Закрыть</button>
            </div>
        </div>
    </div>

    <div class="remodal" data-remodal-id="ecoPriceModal">
        <div class="content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button data-remodal-action="cancel" class="close">×</button>
                <h4 class="modal-title blue">ЭКОНОМ - все самое необходимое по приемлемой цене</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body text-left">
                <ul class="text-left">
                    <li>Выезд специалиста по указанному адресу</li>
                    <li>Разбор техники</li>
                    <li>Чистка от пыли</li>
                    <li>Замена термо-интерфейсов, термо-пасты, термо-прокладок</li>
                    <li>Настройка операционной системы</li>
                    <li>Регистрация пользователя</li>
                    <li>Установка интернет-браузера</li>
                    <li>Установка видео и аудио кодеков</li>
                    <li>Гарантия на услуги 1 месяц</li>
                </ul>
                <br>
                <p><small>Все цены указаны за работы на одном устройстве</small></p>
                <p><small>Стоимость работ (если это явно не оговаривается) указана без учета стоимости
                    оборудования, комплектующих и коммерческого программного обеспечения</small></p>
                <p><small>* НДС не облагаются</small></p>
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-remodal-action="cancel">Закрыть</button>
            </div>
        </div>
    </div>

    <div class="remodal" data-remodal-id="standartPriceModal">
        <div class="content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button data-remodal-action="cancel" class="close">×</button>
                <h4 class="modal-title blue">СТАНДАРТ - сочетает в себе невысокую цену и оптимальный набор услуг</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body text-left">
                <ul class="text-left">
                    <li>Выезд специалиста по указанному адресу</li>
                    <li>Разбор техники</li>
                    <li>Чистка от пыли</li>
                    <li>Замена термо-интерфейсов, термо-пасты, термо-прокладок</li>
                    <li>Настройка операционной системы</li>
                    <li>Активация операционной системы</li>
                    <li>Регистрация пользователя</li>
                    <li>Установка интернет-браузера</li>
                    <li>Установка видео и аудио кодеков</li>
                    <li>Осуществление настроек пользователя (логина, тем, экрана и т.д.)</li>
                    <li>Установка антивирусного программного обеспечения</li>
                    <li>Установка Офисного пакета</li>
                    <li>Настройка Интернета</li>
                    <li>Установка программ для записи данных на CD и DVD, Blu-Ray и HD-DVD диски</li>
                    <li>Установка Программ для работы с архивами и архивации файлов</li>
                    <li>Установка программ для работы с PDF файлами</li>
                    <li>Настройка и подключение переферийных устройств</li>
                    <li>Гарантия на услуги 3 месяца</li>
                </ul>
                <br>
                <p><small>Все цены указаны за работы на одном устройстве</small></p>
                <p><small>Стоимость работ (если это явно не оговаривается) указана без учета стоимости
                        оборудования, комплектующих и коммерческого программного обеспечения</small></p>
                <p><small>* НДС не облагаются</small></p>
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-remodal-action="cancel">Закрыть</button>
            </div>
        </div>
    </div>

    <div class="remodal" data-remodal-id="premiumPriceModal">
        <div class="content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button data-remodal-action="cancel" class="close">×</button>
                <h4 class="modal-title blue">ПРЕМИУМ - для тех, кто хочет получить максимум от своего компьютера и программного обеспечения</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body text-left">
                <ul class="text-left">
                    <li>Выезд специалиста по указанному адресу</li>
                    <li>Разбор техники</li>
                    <li>Чистка от пыли</li>
                    <li>Замена термо-интерфейсов, термо-пасты, термо-прокладок</li>
                    <li>Настройка операционной системы</li>
                    <li>Активация операционной системы</li>
                    <li>Регистрация пользователя</li>
                    <li>Установка интернет-браузера</li>
                    <li>Установка видео и аудио кодеков</li>
                    <li>Осуществление настроек пользователя (логина, тем, экрана и т.д.)</li>
                    <li>Установка антивирусного программного обеспечения</li>
                    <li>Установка Офисного пакета</li>
                    <li>Настройка Интернета</li>
                    <li>Установка программ для записи данных на CD и DVD, Blu-Ray и HD-DVD диски</li>
                    <li>Установка Программ для работы с архивами и архивации файлов</li>
                    <li>Установка программ для работы с PDF файлами</li>
                    <li>Настройка и подключение переферийных устройств</li>
                    <li>Подключение и настройка веб-камер</li>
                    <li>Установка любых программ, предоставленных клиентом (не более трех)</li>
                    <li>Программы для комфортного чтения книг с компьютера или ноутбука</li>
                    <li>Регистрация на Одноклассниках, Вконтакте</li>
                    <li>Создание точки аварийного восстановления системы</li>
                    <li>Программы для обработки изображений</li>
                    <li>Программы для автоматического переключения языковой раскладки клавиатуры</li>
                    <li>Установка справочника ДубльГИС</li>
                    <li>Оптимизация системы</li>
                    <li>Программы-плеера для просмотра ТВ-каналов на компьютерах и ноутбуках</li>
                    <li>Демонстрация и обучение использованию основных функций и возможностей устройства, советы по эксплуатации</li>
                    <li>Гарантия на услуги 3 месяца</li>
                </ul>
                <br>
                <p><small>Все цены указаны за работы на одном устройстве</small></p>
                <p><small>Стоимость работ (если это явно не оговаривается) указана без учета стоимости
                        оборудования, комплектующих и коммерческого программного обеспечения</small></p>
                <p><small>* НДС не облагаются</small></p>
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-remodal-action="cancel">Закрыть</button>
            </div>
        </div>
    </div>

    <div class="remodal" data-remodal-id="vipPriceModal">
        <div class="content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button data-remodal-action="cancel" class="close">×</button>
                <h4 class="modal-title blue">V.I.P. - самый широкий набор программ и работ с сервисной поддержкой Вашего компьютера в течении 1 года!</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body text-left">
                <ul class="text-left">
                    <li>Выезд специалиста по указанному адресу</li>
                    <li>Разбор техники</li>
                    <li>Чистка от пыли</li>
                    <li>Замена термо-интерфейсов, термо-пасты, термо-прокладок</li>
                    <li>Настройка операционной системы</li>
                    <li>Активация операционной системы</li>
                    <li>Регистрация пользователя</li>
                    <li>Установка интернет-браузера</li>
                    <li>Установка видео и аудио кодеков</li>
                    <li>Осуществление настроек пользователя (логина, тем, экрана и т.д.)</li>
                    <li>Установка антивирусного программного обеспечения</li>
                    <li>Установка Офисного пакета</li>
                    <li>Настройка Интернета</li>
                    <li>Установка программ для записи данных на CD и DVD, Blu-Ray и HD-DVD диски</li>
                    <li>Установка Программ для работы с архивами и архивации файлов</li>
                    <li>Установка программ для работы с PDF файлами</li>
                    <li>Настройка и подключение переферийных устройств</li>
                    <li>Подключение и настройка веб-камер</li>
                    <li>Установка любых программ, предоставленных клиентом (не более трех)</li>
                    <li>Программы для комфортного чтения книг с компьютера или ноутбука</li>
                    <li>Регистрация на Одноклассниках, Вконтакте</li>
                    <li>Создание точки аварийного восстановления системы</li>
                    <li>Программы для обработки изображений</li>
                    <li>Программы для автоматического переключения языковой раскладки клавиатуры</li>
                    <li>Установка справочника ДубльГИС</li>
                    <li>Оптимизация системы</li>
                    <li>Программы-плеера для просмотра ТВ-каналов на компьютерах и ноутбуках</li>
                    <li>Демонстрация и обучение использованию основных функций и возможностей устройства, советы по эксплуатации</li>
                    <li>3 экстренный вызов в месяц(оплачиваются отдельно)</li>
                    <li>Один БЕСПЛАТНЫЙ выезд специалиста на техническое обслуживание (один раз в три месяца)</li>
                    <li>Гарантия на услуги 1 год</li>
                </ul>
                <br>
                <p><small>Все цены указаны за работы на одном устройстве</small></p>
                <p><small>Стоимость работ (если это явно не оговаривается) указана без учета стоимости
                        оборудования, комплектующих и коммерческого программного обеспечения</small></p>
                <p><small>* НДС не облагаются</small></p>
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-remodal-action="cancel">Закрыть</button>
            </div>
        </div>
    </div>

    <div class="remodal" data-remodal-id="contactModal">
        <div class="content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button data-remodal-action="cancel" class="close">×</button>
                <h4 class="modal-title">Заказ обратного звонка</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <form class="main-contact-form" name="contact-form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i
                                                            class="fa fa-user-o fa-fw"></i></span>
                                                <input type="text" name="name" class="clientName form-control"
                                                       placeholder="Ваше имя">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                                                <?php
                                                echo MaskedInput::widget([
                                                    'name' => 'phone',
                                                    'mask' => '+7(999)-999-9999',
                                                    'options' => [
                                                        'placeholder' => '+7(___)-___-____',
                                                        'class' => 'clientPhone form-control',
                                                        'type' => 'tel',
                                                        'required' => 'required'
                                                    ]
                                                ]);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 ">
                                        <div class="form-group">
                                            <label for="description">Опишите проблему</label>
                                            <textarea class="description form-control" rows="2"
                                                      placeholder="По желанию"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn-submit">Отправить заявку</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="remodal remodal-wide" data-remodal-id="serviceModal">
        <div class="content">
            <div class="modal-header">
                <button data-remodal-action="cancel" class="close">×</button>
                <h4 class="modal-title">Информация о услуге</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <div class="row text-center">
                    <div class="col-sm-4">
                        <form class="main-contact-form" name="contact-form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                                <span class="input-group-addon"><i
                                                            class="fa fa-user-o fa-fw"></i></span>
                                            <input type="text" name="name" class="form-control clientName"
                                                   placeholder="Ваше имя">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                                            <?php
                                            echo MaskedInput::widget([
                                                'name' => 'phone',
                                                'mask' => '+7(999)-999-9999',
                                                'options' => [
                                                    'placeholder' => '+7(___)-___-____',
                                                    'class' => 'form-control clientPhone',
                                                    'type' => 'tel',
                                                    'required' => 'required'
                                                ]
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 ">
                                    <div class="form-group">
                                        <label for="description">Опишите проблему</label>
                                        <textarea class="form-control description" rows="2"
                                                  placeholder="По желанию"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn-submit">Отправить заявку</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-8">
                        <h3 class="text-uppercase service-modal-title">Установка Windows, iOS, Android и все необходимые программы и драйвера</h3>
                        <p class="text-left service-modal-description">Операционная система представляет собой своего рода скелет, на котором держатся современные возможности компьютеров. Без нее компьютер всего лишь очень дорогая коробочка и очень обидно, когда она не работает так, как мы этого хотим.</p>

                    </div>
                    <div class="info">
                        <p class="text-right service-modal-price">Стоимость - от 500 руб. *</p>
                    </div>
                </div>
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-remodal-action="cancel">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<header id="home">
    <div class="main-nav navbar-fixed-top">
        <div class="container navigator">
            <div class="navbar-header ">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <h1 class="logo"></h1>
                </a>
                <a href="" class="navbar-brand" data-remodal-target="contactModal">
                    <h1 class="phone"><i class="fa fa-phone" aria-hidden="true"></i><?= $cityProperty['phone'] ?></h1>
                </a>
                <a class="hidden-xs hidden-sm hidden-md navbar-brand" href="/">
                    <h1 class="logo-label"></h1>
                </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="scroll"><a href="#home">Начало</a></li>
                    <li class="scroll"><a href="#services">Услуги</a></li>
                    <li class="scroll"><a href="#about-us">Бонусы</a></li>
                    <li class="scroll"><a href="#contact">Контакты</a></li>
                    <li class="price"><a href="" data-remodal-target="priceModal"><i class="fa fa-rouble"></i> Цены</a></li>
                </ul>
            </div>
        </div>
    </div><!--/#main-nav-->
    <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="6000">
        <ol class="carousel-indicators">
            <li data-target="#home-slider" data-slide-to="0" class="active"></li>
            <li data-target="#home-slider" data-slide-to="1"></li>
            <li data-target="#home-slider" data-slide-to="2"></li>
            <li data-target="#home-slider" data-slide-to="3"></li>
            <li data-target="#home-slider" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <div class="title">
                    <h1 class="white-caption text-center ">Компьютерная помощь в <?= $cityProperty['title'] ?></h1>
                    <h1 class="white-caption text-center">Ремонт и обслуживание компьютеров и
                        ноутбуков</h1>
                </div>
                <div class="caption">
                    <span class="hidden-xs main-title">
                        <small>Обслуживание</small>
                        различных
                        <small>устройств</small>
                    </span>
                    <br>
                    <br>
                    <span class="hidden-xs">Не работает компьютер, ноутбук, принтер или роутер?</span>
                    <br>
                    <span class="hidden-xs">Звоните, мы поможем!</span>

                </div>
                <div class="caption-button">
                    <a class="btn btn-start animated pulse" data-remodal-target="contactModal">Вызов мастера</a>
                </div>
            </div>

            <div class="item">
                <div class="title">
                    <h1 class="white-caption text-center ">Компьютерная помощь в <?= $cityProperty['title'] ?></h1>
                    <h1 class="white-caption text-center">Ремонт и обслуживание компьютеров и ноутбуков</h1>
                </div>
                <div class="caption">
                    <span class="hidden-xs main-title">
                        Современный <small>подход к диагностике</small>
                    </span>
                    <br>
                    <br>
                    <span class="hidden-xs">В своей работе наши специалисты применяют современные</span>
                    <br>
                    <span class="hidden-xs">методы по устранению широкого круга неисправностей</span>
                </div>
                <div class="caption-button">
                    <a class="btn btn-start animated pulse" data-remodal-target="contactModal">Вызов мастера</a>
                </div>
            </div>

            <div class="item">
                <div class="title">
                    <h1 class="white-caption text-center ">Компьютерная помощь в <?= $cityProperty['title'] ?></h1>
                    <h1 class="white-caption text-center">Ремонт и обслуживание компьютеров и ноутбуков</h1>
                </div>
                <div class="caption">
                    <span class="hidden-xs main-title">
                        Качество <small>и</small> надежность
                    </span>
                    <br>
                    <br>
                    <span class="hidden-xs">На все работы наша компания предоставляет гарантию.</span>
                    <br>
                    <span class="hidden-xs">Мы не забываем о своих клиента в трудную минуту!</span>
                </div>
                <div class="caption-button">
                    <a class="btn btn-start animated pulse" data-remodal-target="contactModal">Вызов мастера</a>
                </div>
            </div>

            <div class="item">
                <div class="title">
                    <h1 class="white-caption text-center ">Компьютерная помощь в <?= $cityProperty['title'] ?></h1>
                    <h1 class="white-caption text-center">Ремонт и обслуживание компьютеров и ноутбуков</h1>
                </div>
                <div class="caption">
                    <span class="hidden-xs main-title">
                        Индивидуальный <small>подход</small>
                    </span>
                    <br>
                    <br>
                    <span class="hidden-xs">Наши специалисты подскажут, что необходимо Вашему компьютеру</span>
                    <br>
                    <span class="hidden-xs">Лишние услуги и работы мы не навязываем!</span>
                </div>
                <div class="caption-button">
                    <a class="btn btn-start animated pulse" data-remodal-target="contactModal">Вызов мастера</a>
                </div>
            </div>

            <div class="item">
                <div class="title">
                    <h1 class="white-caption text-center ">Компьютерная помощь в <?= $cityProperty['title'] ?></h1>
                    <h1 class="white-caption text-center">Ремонт и обслуживание компьютеров и ноутбуков</h1>
                </div>
                <div class="caption">
                    <span class="hidden-xs main-title">
                        Удобно <small>для Вас</small>
                    </span>
                    <br>
                    <br>
                    <span class="hidden-xs">Наши специалисты приедут к Вам</span>
                    <br>
                    <span class="hidden-xs">в любое удобное для Вас время</span>
                </div>
                <div class="caption-button">
                    <a class="btn btn-start animated pulse" data-remodal-target="contactModal">Вызов мастера</a>
                </div>
            </div>
        </div>
        <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
        <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>
    </div><!--/#home-slider-->
</header><!--/#home-->

<?= $content ?>

<footer id="footer">
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="container text-center">
            <div class="footer-logo">
                <a><img class="img-responsive" src="images/logo_145x50.png" alt="logo"></a>
            </div>
            <div class="social-icons">
                <ul>
                    <li><a class="envelope" href="mailto:eskp24@gmail.com"><i class="fa fa-envelope"></i></a></li>
                    <li><a class="vkontakte" href="https://vk.com/eskp24"><i class="fa fa-vk"></i></a></li>
                    <li><a class="odnoklassniki" href="https://ok.ru/group/53521203527762"><i
                                    class="fa fa-odnoklassniki"></i></a></li>
                    <!--                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>-->
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p>&copy; 2016 Oxygen Theme.</p>
                </div>
                <div class="col-sm-6">
                    <p class="pull-right">Crafted by <a href="mailto:lutersergei@ya.ru">Luther</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="callback_hyanter hidden-xs" data-remodal-target="contactModal"><img src="images/smartphone.png" class="iphone_next"></div>
<?php $this->endBody() ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter42420564 = new Ya.Metrika({
                    id: 42420564,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks"); </script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/42420564" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript> <!-- /Yandex.Metrika counter -->
</body>
</html>
<?php $this->endPage() ?>
