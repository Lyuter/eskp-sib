<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Orbitron:400|Roboto|Oswald:300',
        'css/font-awesome.min.css',
        'css/main.css',
        'css/animate.min.css',
        'css/remodal.css',
        'css/remodal-default-theme.css',
    ];
    public $js = [
        'http://maps.google.com/maps/api/js?key=AIzaSyAEp8WOEc-QGPRFxY6QTWEKk6FgdG8CNNE',
        'js/wow.min.js',
        'js/mousescroll.js',
        'js/main.js',
        'js/remodal.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
