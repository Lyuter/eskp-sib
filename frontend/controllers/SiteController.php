<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Requests;

/**
 * Site controller
 */
class SiteController extends Controller
{
    const TOMSK = 'tomsk';
    const KRASNOYARSK = 'krasnoyarsk';
    const IRKUTSK = 'irkutsk';
    public $defaultCity= ['title' => 'Красноярске', 'phone' => ' 286-04-41', 'address' => ['г.Красноярск, пр. Молокова, 26 ', '+7(391)286-04-41', 'http://www.eskp-sib.ru'], 'map' => ['56.044129', '92.908394']];

    public $cities  = [self::KRASNOYARSK, self::TOMSK, self::IRKUTSK];
    public $properties = [
        self::KRASNOYARSK => ['title' => 'Красноярске', 'phone' => ' 286-04-41', 'address' => ['г.Красноярск, пр. Молокова, 26 ', '+7(391)286-04-41', 'http://www.krasnoyarsk.eskp-sib.ru'], 'map' => ['56.044129', '92.908394']],
        self::TOMSK => ['title' => 'Томске', 'phone' => ' 28-38-09', 'address' => ['г.Томск, ул. Пушкина, 27 ', '+7(3822)28-38-09', 'http://www.tomsk.eskp-sib.ru'], 'map' => ['56.498927', '84.970080']],
        self::IRKUTSK => ['title' => 'Иркутске', 'phone' => '324-04-55', 'address' => ''],
    ];

    /**
     * @return array|null Параметры компании в определенном городе
     */
    public function getCityProperty()
    {
        $serverName = Yii::$app->getRequest()->serverName;
        foreach ($this->properties as $key=>$value)
        {
            if (stripos($serverName, $key) !== false) return $value;
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $cityProperty = $this->getCityProperty();
        if (!$cityProperty) $cityProperty = $this->defaultCity;
        return $this->render('index', [
            'cityProperty' => $cityProperty
        ]);
    }

    public function actionSendemail()
    {
        $model = new Requests();
        $phone = Yii::$app->request->post('phone');
        $phone_filter = preg_replace('/[^0-9]/', '', $phone);
        if(strlen($phone_filter) !== 11) return 0;
        $phone_replace = substr_replace($phone_filter, '8', 0, 1);
        $name = Yii::$app->request->post('name');
        $description = Yii::$app->request->post('description');
        $destinations = ['luter.sergei@ya.ru', 'alekseydiv@icloud.com', 'eskp24@gmail.com'];
        $cityProperty = $this->getCityProperty();
        if (!$cityProperty) $cityProperty = $this->defaultCity;
        $result =  Yii::$app
            ->mailer //TODO добавить шаблон для Email
            ->compose(
//                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
//                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'robot.eskp-sib.ru'])
            ->setTo($destinations)
            ->setSubject('Новый заказ от ' . Yii::$app->request->post('name') . ', в ' . $cityProperty['title'])
            ->setHtmlBody($name . '. Телефон: ' . '<a href="tel: '. $phone_replace . '">'. $phone_replace . '</a>' . '<p>' . $description .  '</p>')
            ->send();
        $model->name = $name;
        $model->phone = $phone_replace;
        $model->description = $description;
        $model->done = (int)$result;
        $model->save();
        return (int)$result;
    }
}
