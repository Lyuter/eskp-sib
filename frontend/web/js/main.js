jQuery(function($) {


    setInterval (function(){
        $('.callback_hyanter').removeClass('rotate_left');
        $('.callback_hyanter').addClass('rotate_right');
        setTimeout(function() {
            $('.callback_hyanter').removeClass('rotate_right');
            $('.callback_hyanter').addClass('rotate_left');
            setTimeout(function() {
                $('.callback_hyanter').removeClass('rotate_left');
                $('.callback_hyanter').addClass('rotate_right');
            },100);
            setTimeout(function() {
                $('.callback_hyanter').removeClass('rotate_right');
                $('.callback_hyanter').removeClass('rotate_left');
            },300);
        },300);

    }, 1600);

    //Preloader
    var preloader = $('.preloader');
    $(window).load(function(){
        preloader.remove();
    });

    // $('.modal-price').on('click', function () {
    //     $("#modalService").modal('hide').on('hidden.bs.modal', function () {
    //         $("#modalPrice").modal('show');
    //     });
    // });
    //
    // $('.modal-contact').on('click', function () {
    //     $("#modalService").modal('hide').on('hidden.bs.modal', function () {
    //         $("#modalContact").modal('show');
    //     });
    // });
    
    //#main-slider
    var slideHeight;
    var navbarHeight;
    function resizeSlider() {
        slideHeight = $(window).height();
        navbarHeight = $(".navigator").height();
        if (slideHeight < 560)
        {
            $('#home-slider .item').css('height',slideHeight - navbarHeight);
            if(slideHeight < 460)
            {
                $('#home-slider .caption').css('position', 'static');
                $('#home-slider .caption-button').css('position', 'static');
                $('.carousel-indicators').css('display', 'none');

            }
            else {
                $('#home-slider .caption').css('position', 'absolute');
                $('#home-slider .caption-button').css('position', 'absolute');
                $('.carousel-indicators').css('display', 'block');

            }
        }
        else
        {
            $('#home-slider .item').css('height',500);
        }
    }
    $(window).resize(function(){
        resizeSlider();
    });
    resizeSlider();

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function(){
        $('.navbar-toggle:visible').click();
    });

    $('.navbar-collapse ul li.scroll a').on('click', function() {
        if($('.main-nav').hasClass('navbar-fixed-top')) {
            $('html, body').animate({scrollTop: $(this.hash).offset().top - 60}, 1000);
            return false;
        }
        else{
            $('html, body').animate({scrollTop: $(this.hash).offset().top - 100}, 1000);
            return false;
        }
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 60
    });

    $('#tohash').on('click', function(){
        $('html, body').animate({scrollTop: $(this.hash).offset().top - 60}, 1000);
        return false;
    });

    //Initiat WOW JS
    new WOW().init();
    //smoothScroll
    smoothScroll.init();

    //Service popovers
    var services = $('.our-services .col-sm-3');
    // services.popover({
    //     trigger: 'hover',
    //     placement: 'auto top',
    //     container: 'body',
    //     delay: { "show": 100, "hide": 200 },
    //     html: true,
    //     template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    // });

    services.on('click', function(){
        var serviceModal = $('[data-remodal-id=serviceModal]');
        serviceModal.find('.service-modal-title').html($(this).data('title'));
        serviceModal.find('.service-modal-description').html($(this).data('content'));
        serviceModal.find('.service-modal-price').html($(this).data('price'));

        serviceModal = $('[data-remodal-id=serviceModal]').remodal();
        serviceModal.open();
    });

    // Contact form
    var form = $('.main-contact-form');
    form.submit(function(event){
        var clientName = $(this).find('.clientName');
        var clientPhone = $(this).find('.clientPhone');
        var description = $(this).find('.description');
        if (!clientName.val()) clientName.val("Аноним");
        event.preventDefault();
        var form_status = $('<div class="form_status"></div>');
        $.ajax({
            method: "POST",
            url: '/site/sendemail',
            data: "name=" + clientName.val() + "&phone=" + clientPhone.val() + "&description=" + description.val(),
            beforeSend: function(){
                form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Письмо отправляется...</p>').fadeIn() );
            }
        }).done(function(data){
            if (data == "1")
            {
                form_status.html('<p class="bg-success">Запрос успешно отправлен. В ближайшее время оператор Вам перезвонит!</p>').delay(3000).fadeOut();
            }
            else
            {
                form_status.html('<p class="bg-danger">Что-то пошло не так. Пожалуйста, свяжитесь самостоятельно с оператором по телефону 286-04-41</p>');
            }
            setTimeout(function () {
                $('#modalContact').modal('hide');
            }, 3000);
        });
    });

    //Google Map
    var latitude = $('#google-map').data('latitude');
    var longitude = $('#google-map').data('longitude');
    function initialize_map() {
        var myLatlng = new google.maps.LatLng(latitude,longitude);
        var mapOptions = {
            zoom: 14,
            scrollwheel: false,
            center: myLatlng
        };
        var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);

        new google.maps.Marker({
            position: myLatlng,
            map: map
        });

    }
    google.maps.event.addDomListener(window, 'load', initialize_map);

});
