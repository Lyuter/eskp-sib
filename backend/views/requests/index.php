<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RequestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Входящие запросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requests-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'date',
            'name:ntext',
            [
                'attribute' => 'phone',
                'format' => 'raw',
                'value' => function($data)
                {
                    return Html::a($data->phone, "tel:$data->phone");
                }
            ],
            'description',
            // 'done',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{add}',
                'buttons' => [
                    'add' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['contact', 'id' => $model->id], [
                            'class' => '',
                            'data' => [
                                'confirm' => 'Добавить в Контакты?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
