<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Contractors;
use common\models\Customers;
use common\models\Services;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'customer_id')->dropDownList(
        ArrayHelper::map(Customers::find()->all(), 'id', 'fullname'),
        [
            'prompt' => 'Укажите клиента'
        ]
    );
    ?>

    <?=
     $form->field($model, 'contractor_id')->dropDownList(
        ArrayHelper::map(Contractors::find()->all(), 'id', 'fullname'),
         [
            'prompt' => 'Укажите мастера'
         ]
    );
    ?>

    <div class="form-group field-date_add">
        <?php
        echo '<label class="control-label" for="date_add">Дата добавления</label>';
        echo DateTimePicker::widget([
            'id' => 'date_add',
            'name' => 'Orders[date_add]',
            'value' => $model->date_add,
            'options' => ['placeholder' => 'Укажите время'],
            'pluginOptions' => [
                'format' => "yyyy-mm-dd hh:ii",
                'todayHighlight' => true
            ]
        ]);
        ?>
    </div>

    <div class="form-group field-date_complete">
        <?php
        echo '<label class="control-label" for="date_complete">Дата завершения</label>';
        echo DateTimePicker::widget([
            'id' => 'date_complete',
            'name' => 'Orders[date_complete]',
            'value' => $model->date_complete,
            'options' => ['placeholder' => 'Укажите время'],
            'pluginOptions' => [
                'format' => "yyyy-mm-dd hh:ii",
                'todayHighlight' => true
            ]
        ]);
        ?>
    </div>

    <div class="form-group field-services">
        <?php
        $services = array();
        foreach ($model->ordersServices as $service)
        {
            $services[] = $service->service_id;
        }
        echo '<label class="control-label">Проделанные работы</label>';
        echo Select2::widget([
            'language' => 'ru',
            'name' => 'Services',
            'value' =>$services, // initial value
            'data' => ArrayHelper::map(Services::find()->all(), 'id', 'description'),
            'maintainOrder' => true,
            'options' => ['placeholder' => 'Укажите услуги', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 50
            ],
        ]);
        ?>
    </div>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'salary')->textInput() ?>

    <?= $form->field($model, 'done')->widget(SwitchInput::classname(), []) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
