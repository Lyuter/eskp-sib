<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use jino5577\daterangepicker\DateRangePicker;
use backend\components\NumberColumn;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('Добавление заказа', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'customer',
                'value' => 'customer.fullname',
                'label' => 'Заказчик'
            ],
            [
                'attribute' => 'contractor',
                'value' => 'contractor.fullname',
                'label' => 'Мастер'
            ],
            [
                'attribute' => 'date_add',
                'format' => ['date', 'php:Y-m-d'],
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'dateRange',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'YYYY-MM-DD',
                        ],
                        'autoUpdateInput' => false,
                        "showDropdowns" => true,
                    ]
                ])
            ],
            [
                'label' => 'Услуги',
                'format' => 'ntext',
                'attribute'=>'services',
                'value' => function($model) {
                    if ($model->services) {
                        foreach ($model->services as $service) {
                            $services[] = $service->description;
                        }
                        return implode("\n", $services);
                    }
                    return null;
                },
            ],
            [
                'label' => 'Цена',
                'attribute'=>'price',
                'format' => 'currency',
                'class' => NumberColumn::className(),
            ],
            [
                'label' => 'Мастеру',
                'attribute'=>'salary',
                'format' => 'currency',
                'class' => NumberColumn::className(),
            ],
            [
                'label' => 'Выполнено?',
                'attribute'=>'done',
                'value' => function($model)
                {
                    if ($model->done) return 'Да';
                    return 'Нет';
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
