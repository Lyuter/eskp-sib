<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить заказ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'customer.fullname',
                'label' => 'Заказчик'
            ],
            [
                'attribute' => 'contractor.fullname',
                'label' => 'Мастер'
            ],
            'date_add',
            'date_complete',
            [
                'label' => 'Сервисы',
                'format' => 'ntext',
                'attribute'=>'services',
                'value' => function($model) {
                    if ($model->services) {
                        foreach ($model->services as $service) {
                            $services[] = $service->description;
                        }
                        return implode("\n", $services);
                    }
                    return null;
                },
            ],
            'price',
            'salary',
            [
                'label' => 'Выполнено?',
                'attribute'=>'done',
                'value' => function($model)
                {
                    if ($model->done) return 'Да';
                    return 'Нет';
                }
            ],
        ],
    ]) ?>

</div>
