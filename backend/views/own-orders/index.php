<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use jino5577\daterangepicker\DateRangePicker;
use backend\components\NumberColumn;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OwnOrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showFooter' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'customer',
                    'value' => 'customer.fullname',
                    'label' => 'Заказчик'
                ],
                [
                    'attribute' => 'customer',
                    'value' => 'customer.address',
                    'label' => 'Адрес'
                ],
                [
                    'attribute' => 'date_add',
                    'format' => ['date', 'php:Y-m-d'],
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'dateRange',
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'YYYY-MM-DD',
                            ],
                            'autoUpdateInput' => false,
                            "showDropdowns" => true,
                        ]
                    ])
                ],
//                [
//                    'attribute' => 'date_add',
//                    'label' => 'Дата добавления',
//                    'format' => ['date', 'php:Y-m-d']
//                ],
//                [
//                    'attribute' => 'date_complete',
//                    'label' => 'Дата завершения',
//                    'format' => ['date', 'php:Y-m-d']
//                ],
                [
                    'label' => 'Услуги',
                    'format' => 'ntext',
                    'attribute'=>'services',
                    'value' => function($model) {
                        if ($model->services) {
                            foreach ($model->services as $service) {
                                $services[] = $service->description;
                            }
                            return implode("\n", $services);
                        }
                        return null;
                    },
                ],
                [
                    'label' => 'Цена',
                    'attribute'=>'price',
                    'format' => 'currency',
                    'class' => NumberColumn::className(),
                ],
                [
                    'label' => 'Себе',
                    'attribute'=>'salary',
                    'format' => 'currency',
                    'class' => NumberColumn::className(),
                ],
                [
                    'label' => 'Выполнено?',
                    'attribute'=>'done',
                    'value' => function($model)
                    {
                        if ($model->done) return 'Да';
                        return 'Нет';
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
<div class="row">
    <div class="col-xs-4 col-xs-offset-4">
        <?= Html::a('Добавить заказ',['own-orders/create'], ['class' => 'btn btn-lg btn-success btn-block']);?>
    </div>
</div>
