<?php

/* @var $this yii\web\View */
/* @var $countRequest integer */
/* @var $countCustomers integer */
/* @var $countContractors integer */
/* @var $countOrders integer */

use yii\helpers\Url;

$this->title = 'ЦУП';
?>
<div class="row">
    <div class="col-lg-3 col-lg-offset-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= $countRequest ?></h3>

                <p class="info-box-text">Запросы позвонить</p>
            </div>
            <div class="icon">
                <i class="fa fa-volume-control-phone"></i>
            </div>
            <a href="<?= Url::to(['requests/index']) ?>" class="small-box-footer">
                Больше информации <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= $countCustomers ?></h3>

                <p class="info-box-text">Клиенты</p>
            </div>
            <div class="icon">
                <i class="fa fa-address-book"></i>
            </div>
            <a href="<?= Url::to(['customers/index']) ?>" class="small-box-footer">
                Больше информации <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-lg-offset-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?= $countContractors ?></h3>

                <p class="info-box-text">Мастера</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-secret"></i>
            </div>
            <a href="<?= Url::to(['contractors/index']) ?>" class="small-box-footer">
                Больше информации <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?= $countOrders ?></h3>

                <p class="info-box-text">Заказы</p>
            </div>
            <div class="icon">
                <i class="fa fa-line-chart"></i>
            </div>
            <a href="<?= Url::to(['orders/index']) ?>" class="small-box-footer">
                Больше информации <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div>
<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-xs-12">
        <div id="map" style="width: 100%; height: 400px"></div>
    </div>
</div>
