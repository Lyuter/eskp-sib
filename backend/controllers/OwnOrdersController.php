<?php

namespace backend\controllers;

use common\models\Customers;
use common\models\OrdersServices;
use common\models\Services;
use common\models\User;
use Yii;
use common\models\Orders;
use common\models\OwnOrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OwnOrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'update', 'delete', 'view', 'add-customer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OwnOrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Create new own order.
     * If creation is successful, the browser will be redirected to the 'own statistics' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $customer = new Customers();
        $model = new Orders();
        if ($model->load(Yii::$app->request->post())) {

            $model->contractor_id = User::getContractor()->id;
            $model->save();
            //Link and create new services if not exist
            $services = Yii::$app->request->post('Services');
            if ($services)
            {
                foreach ($services as $service_id)
                {
                    if (!$service = Services::findOne($service_id))
                    {
                        $service = new Services();
                        $service->description = $service_id;
                        $service->save();
                    }
                    $model->link('services',$service);
                }
            }
            $model->save();
            return $this->redirect(['own-orders/']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'customer' => $customer
            ]);
        }
    }

    public function actionViewStatistics()
    {

    }

    /**
     * Create new Customer
     */
    public function actionAddCustomer()
    {
        $customer = new Customers();
        if ($customer->load(Yii::$app->request->post()) && $customer->save()) {
            Yii::$app->session->setFlash('success', 'Заказчик успешно добавлен!');
            return $this->redirect(['own-orders/create']);
        }
        Yii::$app->session->setFlash('warning', 'Заказчик не добавлен!');
        return $this->redirect(['own-orders/create']);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $services = Yii::$app->request->post('Services');
            OrdersServices::deleteAll(['order_id' => $model->id]);
            if ($services)
            {
                foreach ($services as $service_id)
                {
                    if (!$service = Services::findOne($service_id))
                    {
                        $service = new Services();
                        $service->description = $service_id;
                        $service->save();
                    }
                    $model->link('services',$service);
                }
            }
            return $this->redirect(['own-orders/view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['own-orders/index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
