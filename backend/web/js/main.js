jQuery(function($) {

    function init() {
        var myMap = new ymaps.Map("map", {
                center: [92.888594, 56.022906],
                zoom: 11
            }, {
                searchControlProvider: 'yandex#search'
            });
        var myGeoObjects = [];

        for (var i = 0; i<coordinates.length; i++) {
            myGeoObjects[i] = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: coordinates[i]
                },
                properties: {
                    balloonContentHeader: names[i],
                    balloonContentBody: phones[i]
                }
            });
        }

        var myClusterer = new ymaps.Clusterer();

        myClusterer.add(myGeoObjects);
        myMap.geoObjects.add(myClusterer);
    }

    var coordinates = [];
    var phones = [];
    var names =[];

    $.ajax({
        method: "GET",
        url: '/ajax'
    }).done(function(data){
        $.each(data,function(index,value){
            var coord = value.coordinates.split(" ");
            coordinates.push(coord);
            phones.push(value.phone);
            names.push(value.fullname);
        });
        ymaps.ready(init);
    });
});